package jdprice.abc;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class GetPrice {
	static final String akabotang = "http://item.yiyaojd.com/3083223.html";
	static final String boliwei = "https://item.yiyaojd.com/3336773.html";
	static final String baolindanjianna = "https://item.yiyaojd.com/4158898.html";
	static final String afatuotading28 = "https://item.yiyaojd.com/100002375970.html";
	static final String afatuotading7 = "https://item.yiyaojd.com/4309784.html";
	static final String pifatading = "https://item.yiyaojd.com/4232688.html";
	static final String xueshuanxinmaining = "https://item.yiyaojd.com/4217516.html";
	static final String as="https://item.jd.com/526472.html";
	WebDriver driver;

	public GetPrice() {
		initDriver();

	}

	public void initDriver() {
		try {

			DesiredCapabilities des = new DesiredCapabilities("chrome", "", org.openqa.selenium.Platform.LINUX);
			des.setCapability("acceptInsecureCerts", true);
			des.setCapability("marionette", true);
			des.setCapability("moz:webdriverClick", false); // workaround for firefox upload
			this.driver = new RemoteWebDriver(new URL("http://" + "192.169.2.8:4444" + "/wd/hub/"), des);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().fullscreen();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}

	public static Goods getPrice(String url) {
		Goods goods = new Goods();
		GetPrice g = null;
		try {
			g = new GetPrice();
			g.driver.get(url);
			Thread.sleep(5000);
			String name = g.driver.findElement(org.openqa.selenium.By.xpath(".//div[@class='sku-name']")).getText();
			String price = g.driver
					.findElement(org.openqa.selenium.By.xpath(".//div[contains(@class,'summary-price-wrap')]"))
					.findElement(org.openqa.selenium.By.xpath(".//span[starts-with(@class,'price')]")).getText();
			String plus_price = g.driver.findElement(org.openqa.selenium.By.xpath(".//span[@class='p-price-plus']"))
					.getText();
			String quans = "";
			try {
				List<WebElement> quanItems = g.driver.findElement(org.openqa.selenium.By.id("summary-quan"))
						.findElements(org.openqa.selenium.By.xpath(".//span[@class='quan-item']"));
				for (WebElement item : quanItems) {
					String quan = item.findElement(org.openqa.selenium.By.xpath(".//span")).getText();
					quans = quans + quan + ";";
				}
			} catch (Exception e) {
			}
			System.out.print(name.trim());
			System.out.println(price.trim());
			System.out.println(plus_price.trim());
			System.out.println(quans.trim().endsWith(";"));
			goods.setName(name.trim());
			goods.setPrice(price.trim());
			goods.setPlus_price(plus_price.trim());
			goods.setCoupons(quans.trim().endsWith(";")?quans.substring(0,quans.length()-1):quans);
			goods.setUrl(url.trim());
			return goods;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (g != null) {
				g.driver.quit();
			}
		}
		return null;
	}

	public static void main(String[] args) {
		List<Goods> goods = new ArrayList<Goods>();
		List<String> goodsUrl = new ArrayList<String>();
//		String[] all = { akabotang, boliwei, baolindanjianna, afatuotading28, afatuotading7, pifatading,
//				xueshuanxinmaining ,as};
		String[] all = { as};
		if (args.length == 0) {

			goodsUrl = java.util.Arrays.asList(all);
		} else {
			goodsUrl = java.util.Arrays.asList(args);
		}
		Long t = System.currentTimeMillis();
		for (String a : goodsUrl) {
			Goods g = getPrice(a);
			g.setTimestamp(t);
			goods.add(g);
			JdbcUtils.insert(g);
		}

	}
}
