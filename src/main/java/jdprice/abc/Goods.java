package jdprice.abc;

import java.util.ArrayList;
import java.util.List;

public class Goods {
	String url;
	String name;
	String price;
	String plus_price;
	String coupons;
	String Supplier;
	boolean inStock;
	Long timestamp;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPlus_price() {
		return plus_price;
	}

	public void setPlus_price(String plus_price) {
		this.plus_price = plus_price;
	}

	public String getCoupons() {
		return coupons;
	}

	public void setCoupons(String coupons) {
		this.coupons = coupons;
	}

	public String getSupplier() {
		return Supplier;
	}

	public void setSupplier(String supplier) {
		Supplier = supplier;
	}

	public boolean isInStock() {
		return inStock;
	}

	public void setInStock(boolean inStock) {
		this.inStock = inStock;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Double getBestPrice() {
		Double _price = 10000000d;
		Double _plusprice = 10000000d;
		if (this.plus_price != null && !"".equals(this.plus_price)) {
			_plusprice = Double.parseDouble(this.plus_price);
		}

		if (this.price != null && !"".equals(this.price)) {
			_price = Double.parseDouble(this.price);
		}
		List<String> couponList = new ArrayList<String>();
		if (this.coupons != null) {
			couponList = java.util.Arrays.asList(this.coupons.split(";"));
		}
		Double bestp = -1D;
		Double bestpp = -1D;
		if (couponList.isEmpty()) {
			return _price > _plusprice ? _plusprice : _price;
		}
		for (String c : couponList) {
			if("".equals(c)) {
				continue;
			}
			Double cp = discountPrice(this.price, c);
			if (bestp < 0 || bestp > cp) {
				bestp = cp;
			}
			if (this.plus_price != null && !"".equals(this.plus_price)) {
				Double cpp = discountPrice(this.plus_price, c);
				if (bestpp < 0 || bestpp > cpp) {
					bestpp = cpp;
				}
			}
		}

		if (bestpp == -1) {
			return bestp;
		} else {
			return bestp > bestpp ? bestpp : bestp;

		}
	}

	private Coupon getCp(String input) {
		Coupon c = new Coupon();
		String[] m = input.replace("每满", "").replace("满", "").split("减");
		c.setReduce(m[1]);
		c.setTotal(m[0]);
		return c;
	}

	private Double discountPrice(String price, String coupon) {
		int count = 0;
		Coupon currrentc = getCp(coupon);
		Double dp = Double.parseDouble(price);
		Double all = Double.parseDouble(currrentc.getTotal());
		Double reduce = Double.parseDouble(currrentc.getReduce());
		double i = all % dp;
		if (i != 0) {
			count = ((Double) (all / dp)).intValue() + 1;

		} else {
			count = ((Double) (all / dp)).intValue();
		}
		double totalPrice = dp * count - reduce;

		return totalPrice / count;
	}

	public static void main(String[] args) {
		int count = 0;
		Double dp = Double.parseDouble("30.00");
		Double all = Double.parseDouble("10");
		Double reduce = Double.parseDouble("20");
		double i = all % dp;
		if (i != 0) {
			count = ((Double) (all / dp)).intValue() + 1;

		} else {
			count = ((Double) (all / dp)).intValue();
		}

		System.out.print(count);
	}

}
