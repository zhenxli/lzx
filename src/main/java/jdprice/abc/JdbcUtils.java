package jdprice.abc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JdbcUtils {

	private static Connection getConn() {
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:23306/mydb";
		String username = "root";
		String password = "root";
		Connection conn = null;
		try {
			Class.forName(driver); // classLoader,加载对应驱动
			conn = (Connection) DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static int insert(Goods goods) {
		Connection conn = getConn();
		int i = 0;
		String sql = "insert into goods (name,price,plus_price,coupons,supplier,timestamp,url,discountprice) values(?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt;
		try {
			pstmt = (PreparedStatement) conn.prepareStatement(sql);
			pstmt.setString(1, goods.getName());
			pstmt.setString(2, goods.getPrice());
			pstmt.setString(3, goods.getPlus_price());
			pstmt.setString(4, goods.getCoupons());
			pstmt.setString(5, goods.getSupplier());
			pstmt.setTimestamp(6, new java.sql.Timestamp(goods.getTimestamp()));
			pstmt.setString(7, goods.getUrl());
			pstmt.setString(8, goods.getBestPrice()+"");
			i = pstmt.executeUpdate();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
}
